<?php 


/* Posts 2 Posts
******************************/

// post_2_posts plugin
// https://github.com/scribu/wp-posts-to-posts/wiki/Basic-usage


function my_connection_types() {
	// Make sure the Posts 2 Posts plugin is active.
	if ( !function_exists( 'p2p_register_connection_type' ) )
		return;

	p2p_register_connection_type( array(
		'name' => 'bcf_p2p_trad',
		'from' => array('post','page', 'director'),
		'to' =>  array('post','page', 'director'),
		'reciprocal' => true,
		'title' => 'Traductions',
		// 'admin_box' => 'from',
	) );

}
add_action( 'init', 'my_connection_types', 100 );

