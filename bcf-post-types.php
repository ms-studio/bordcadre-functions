<?php 



/* Register Post Types
 ********************
*/

add_action( 'init', 'bcf_register_post_types' );

function bcf_register_post_types() {

		register_post_type(
				'news', array(	
					'label' => __( 'News' ),
					//'description' => 'Les films produits par Bord Cadre',
					'public' => true,
					'show_ui' => true,
					'show_in_menu' => true,
					 'menu_icon' => 'dashicons-admin-post', // src: http://melchoyce.github.io/dashicons/
					// dashicons-admin-post
					'capability_type' => 'post',
					'hierarchical' => false,
					'has_archive'		 => false,
					'rewrite' => array('slug' => ''),
					'query_var' => true,
					'exclude_from_search' => false,
					'menu_position' => 6,
					'supports' => array(
						'title',
						'editor',
						'revisions',
						'thumbnail',
						'author',
						'publicize',
						),
					'taxonomies' => array( 'category', 'news-type' ), // 'post_tag',
					'labels' => array (
				  	  'name' => 'News',
				  	  'singular_name' => 'News',
				  	  'menu_name' => 'News',
				  	  'add_new' => 'Ajouter',
				  	  'add_new_item' => 'Ajouter une News',
				  	  'edit' => 'Modifier',
				  	  'edit_item' => 'Modifier la News',
				  	  'new_item' => 'Nouvelle News',
				  	  'view' => 'Afficher',
				  	  'view_item' => 'Afficher la News',
				  	  'search_items' => 'Rechercher',
				  	  'not_found' => 'Aucun résultat',
				  	  'not_found_in_trash' => 'Aucun résultat',
				  	  'parent' => 'Élément Parent',
				),
			) 
		);
		
		
		register_post_type(
				'director', array(	
					'label' => __( 'Director', 'promenade' ),
					//'description' => 'Les films produits par Bord Cadre',
					'public' => true,
					'show_ui' => true,
					'show_in_menu' => true,
					 'menu_icon' => 'dashicons-businessman', // src: http://melchoyce.github.io/dashicons/
					// dashicons-admin-post
					'capability_type' => 'post',
					'hierarchical' => false,
					'has_archive'		 => false,
					'rewrite' => array('slug' => ''),
					'query_var' => true,
					'exclude_from_search' => true,
					'menu_position' => 6,
					'supports' => array(
						'title',
						'editor',
						'revisions',
						'thumbnail',
						),
					 'taxonomies' => array( 'lang' ),
					'labels' => array (
				  	  'name' => __( 'Directors', 'promenade' ),
				  	  'singular_name' => __( 'Director', 'promenade' ),
				  	  'menu_name' => __( 'Directors', 'promenade' ),
				  	  'add_new' => 'Ajouter',
				  	  'add_new_item' => 'Ajouter un Réalisateur',
				  	  'edit' => 'Modifier',
				  	  'edit_item' => 'Modifier le Réalisateur',
				  	  'new_item' => 'Nouveau Réalisateur',
				  	  'view' => 'Afficher',
				  	  'view_item' => 'Afficher le Réalisateur',
				  	  'search_items' => 'Rechercher',
				  	  'not_found' => 'Aucun résultat',
				  	  'not_found_in_trash' => 'Aucun résultat',
				  	  'parent' => 'Élément Parent',
				),
			) 
		);
		
		
	// Add a Language Taxonomy
					
				register_taxonomy('lang',
						array( 'post', 'page', 'director', 'news' ), // post = film
						array( 
				 		'hierarchical' => true, 
				 		'label' => 'Langue',
				 		'labels'  => array(
				 			'name'                => _x( 'Langues', 'taxonomy general name' ),
				 			'singular_name'       => _x( 'Langue', 'taxonomy singular name' ),
				 			'search_items'        => __( 'Chercher parmi les langues' ),
				 			'popular_items'              => __( 'Les plus utilisées' ),
				 					'all_items'                  => __( 'Toutes les langues' ),
				 					'parent_item'                => null,
				 					'parent_item_colon'          => null,
				 					'edit_item'                  => __( 'Modifier la langue' ),
				 					'update_item'                => __( 'Mettre à jour la langue' ),
				 					'add_new_item'               => __( 'Nouvelle langues' ),
				 					'new_item_name'              => __( 'Nouvelle langues' ),
				 					'separate_items_with_commas' => __( 'Séparez les langues par des virgules' ),
				 					'add_or_remove_items'        => __( 'Ajouter ou supprimer des langues' ),
				 					'choose_from_most_used'      => __( 'Choisir parmi les langues les plus utilisés' ),
				 					'not_found'                  => __( 'Aucune langue trouvée.' ),
				 			'menu_name'           => __( 'Langues' )
				 		),
				 		'show_ui' => true,
				 		'query_var' => true,
				 		'rewrite' => array('slug' => 'lang'),
				 		'singular_label' => 'Langue') 
				 );	
				
				
				register_taxonomy('news-type',
								array( 'news' ),
								array( 
						 		'hierarchical' => true, 
						 		'label' => 'News Type',
						 		'labels'  => array(
						 			'name'                => _x( 'News Type', 'taxonomy general name' ),
						 			'singular_name'       => _x( 'News Type', 'taxonomy singular name' ),
						 			'search_items'        => __( 'Chercher parmi les News Type' ),
						 			'popular_items'              => __( 'Les plus utilisés' ),
						 					'all_items'                  => __( 'Toutes les News Type' ),
						 					'parent_item'                => null,
						 					'parent_item_colon'          => null,
						 					'edit_item'                  => __( 'Modifier' ),
						 					'update_item'                => __( 'Mettre à jour' ),
						 					'add_new_item'               => __( 'Nouveau News Type' ),
						 					'new_item_name'              => __( 'Nouveau News Type' ),
						 					'separate_items_with_commas' => __( 'Séparez les News Type par des virgules' ),
						 					'add_or_remove_items'        => __( 'Ajouter ou supprimer' ),
						 					'choose_from_most_used'      => __( 'Choisir parmi les plus utilisés' ),
						 					'not_found'                  => __( 'Aucune News Type trouvée.' ),
						 			'menu_name'           => __( 'News Type' )
						 		),
						 		'taxonomies' => array( 'news-type', 'lang' ),
						 		'show_ui' => true,
						 		'query_var' => true,
						 		'rewrite' => array('slug' => 'news-type'),
						 		'singular_label' => 'News Type') 
						 );	
				
		
		

}


