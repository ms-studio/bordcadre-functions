<?php

/* Jetpack Stuff
* see: http://jeremyherve.com/2013/11/19/customize-the-list-of-modules-available-in-jetpack/
 * Disable all non-whitelisted jetpack modules.
 *
 * This will allow all of the currently available Jetpack modules to work
 * normally. If there's a module you'd like to disable, simply comment it out
 * or remove it from the whitelist and it will no longer load.
 *
 * @author FAT Media, LLC
 * @link   http://wpbacon.com/tutorials/disable-jetpack-modules/
 */
 

function bcf_remove_jetpack() {
	if( class_exists( 'Jetpack' ) && !current_user_can( 'manage_options' ) ) {
		remove_menu_page( 'jetpack' );
	}
}
add_action( 'admin_init', 'bcf_remove_jetpack' );



 
 function bordcadre_jetpacks( $modules ) {
 	// A list of Jetpack modules which are allowed to activate.
 	$whitelist = array(
// 		'after-the-deadline',
 		'carousel',
// 		'comments',
// 		'contact-form',
// 		'custom-css',
 		'enhanced-distribution',
// 		'gplus-authorship',
// 		'gravatar-hovercards',
// 		'infinite-scroll',
// 		'json-api',
// 		'latex',
// 		'likes',
//		'markdown',
// 		'minileven',
// 		'mobile-push',
 		'manage',
 		'monitor',
// 		'notes',
// 		'omnisearch',
// 		'photon',
// 		'post-by-email',
 		'publicize',
// 		'sharedaddy',
// 		'shortcodes',
// 		'shortlinks',
// 		'sso',
 		'stats',
// 		'subscriptions',
 		'tiled-gallery',
// 		'vaultpress',
// 		'videopress',
 		'widget-visibility',
// 		'widgets',
 	);
 	// Deactivate all non-whitelisted modules.
 	$modules = array_intersect_key( $modules, array_flip( $whitelist ) );
 	return $modules;
}

  add_filter( 'jetpack_get_available_modules', 'bordcadre_jetpacks' );
  
  add_filter( 'jetpack_get_default_modules', 'bordcadre_jetpacks' );
  
  
