<?php
/*
Plugin Name: Bord Cadre Functions
Plugin URI: https://bitbucket.org/ms-studio/bordcadre-functions/
Description: This plugin adds functionality to the Bord Cadre website.
Version: 1.0.0
Author: Manuel Schmalstieg
Author URI: http://ms-studio.net
*/




/* Relabel Posts to Films
 ********************
*/

function bcf_change_post_label() {
    global $menu;
    global $submenu;
    $menu[5][0] = 'Films';
    $submenu['edit.php'][5][0] = 'Films';
    $submenu['edit.php'][10][0] = 'Add Films';
    // $submenu['edit.php'][16][0] = 'Film Tags';
    echo '';
}
function bcf_change_post_object() {
    global $wp_post_types;
    $labels = &$wp_post_types['post']->labels;
    $labels->name = 'Films';
    $labels->singular_name = 'Film';
    $labels->add_new = 'Add Film';
    $labels->add_new_item = 'Add Film';
    $labels->edit_item = 'Edit Film';
    $labels->new_item = 'Film';
    $labels->view_item = 'View Film';
    $labels->search_items = 'Search Films';
    $labels->not_found = 'No Films found';
    $labels->not_found_in_trash = 'No Films found in Trash';
    $labels->all_items = 'All Films';
    $labels->menu_name = 'Films';
    $labels->name_admin_bar = 'Films';
}
 
add_action( 'admin_menu', 'bcf_change_post_label' );
add_action( 'init', 'bcf_change_post_object' );


function bcf_replace_admin_menu_icons_css() {
    ?>
    <style>
        #menu-posts .dashicons-admin-post:before {
        	content: "\f126";
        }
    </style>
    <?php
}

add_action( 'admin_head', 'bcf_replace_admin_menu_icons_css' );


// Localize
include_once (plugin_dir_path(__FILE__).'bcf-multilingual.php');

// Widgets
include_once (plugin_dir_path(__FILE__).'bcf-promenade-overrides.php');

// Post Types
include_once (plugin_dir_path(__FILE__).'bcf-post-types.php');


// p2p
include_once (plugin_dir_path(__FILE__).'bcf-p2p.php');

// Jetpack
include_once (plugin_dir_path(__FILE__).'bcf-jetpack.php');



/* Allowed FileTypes
 ********************
 * method based on 
 * http://howto.blbosti.com/?p=329
 * List of defaults: https://core.trac.wordpress.org/browser/tags/3.8.1/src/wp-includes/functions.php#L1948
*/

add_filter('upload_mimes', 'custom_upload_mimes');
function custom_upload_mimes ( $existing_mimes=array() ) {

		// add your extension to the array
		$existing_mimes['svg'] = 'image/svg+xml';

		// removing existing file types
		unset( $existing_mimes['bmp'] );
		unset( $existing_mimes['tif|tiff'] );

		// and return the new full result
		return $existing_mimes;
}




/*
 * File Upload Security
 
 * Sources: 
 * http://www.geekpress.fr/wordpress/astuce/suppression-accents-media-1903/
 * https://gist.github.com/herewithme/7704370
 
 * See also Ticket #22363
 * https://core.trac.wordpress.org/ticket/22363
 * and #24661 - remove_accents is not removing combining accents
 * https://core.trac.wordpress.org/ticket/24661
*/ 

add_filter( 'sanitize_file_name', 'remove_accents', 10, 1 );
add_filter( 'sanitize_file_name_chars', 'sanitize_file_name_chars', 10, 1 );
 
function sanitize_file_name_chars( $special_chars = array() ) {
	$special_chars = array_merge( array( '’', '‘', '“', '”', '«', '»', '‹', '›', '—', 'æ', 'œ', '€','é','à','ç','ä','ö','ü','ï','û','ô','è' ), $special_chars );
	return $special_chars;
}


/*
 * gallery shortcode improvement
 * makes it link to the "large" file (not full size) 
 
 * http://oikos.org.uk/2011/09/tech-notes-using-resized-images-in-wordpress-galleries-and-lightboxes/
*/

function bcf_get_attachment_link_filter( $content, $post_id, $size, $permalink ) {
 
    // Only do this if we're getting the file URL
    if (! $permalink) {
        // This returns an array of (url, width, height)
        $image = wp_get_attachment_image_src( $post_id, 'large' );
        $new_content = preg_replace('/href=\'(.*?)\'/', 'href=\'' . $image[0] . '\'', $content );
        return $new_content;
    } else {
        return $content;
    }
}
 
add_filter('wp_get_attachment_link', 'bcf_get_attachment_link_filter', 10, 4);



  
  