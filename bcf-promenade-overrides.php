<?php 

function promenade_hero_image() {
	// Display featured image on the first page only.

	// do nothing
	
}


function promenade_site_title() {
	$site_logo = promenade_theme()->logo->html();
	
	$site_title_url = '';

	$site_title = sprintf( '<h1 class="site-title"><a href="%1$s" rel="home">%2$s</a></h1>',
		esc_url( home_url( '/' ) ),
		get_bloginfo( 'name' )
	);

	echo $site_logo . $site_title;
}




/* footer widget zone */

//function promenade_credits() {
//	$text = 'Bord Cadre Films';
//	echo apply_filters( 'promenade_credits', $text );
//}

