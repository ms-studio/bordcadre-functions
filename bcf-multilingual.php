<?php


// Define locale based on URL / post type slug
function mimu_redefine_locale($locale) {

	// $mimu_urlpath = explode("/", $_SERVER['REQUEST_URI']);
	
	$mimu_urlpath = $_SERVER['REQUEST_URI'];

	// we want to test for "-en/"
	
	$mimu_url_lang = substr($mimu_urlpath, -4);
	
	// $mimu_url_lang = $mimu_urlpath[1];
		if ( $mimu_urlpath == "/home/" ) {
			$locale = 'en_US';
		} else if ( $mimu_url_lang == "-en/") {
			$locale = 'en_US';
		} else { // default = load french
			$locale = 'fr_FR';
		}
  return $locale;
}
add_filter('locale','mimu_redefine_locale',10);

/*
 * Note: can be tested in theme with
 $my_current_language = get_bloginfo('language');
*/


/*
 * Define language by taxonomy
*/
function bcf_lang_tax() {
  
  $bcf_lang_var = "fr"; // default value
    
  if ( is_singular() ) {
  	if ( has_term( 'en', 'lang' ) ) {
  		$bcf_lang_var = "en";
  	}
  }
  return $bcf_lang_var;
}